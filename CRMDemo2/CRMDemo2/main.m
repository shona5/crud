//
//  main.m
//  CRMDemo2
//
//  Created by Don Asok on 02/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
