//
//  LoginViewController.m
//  CRM
//
//  Created by Don Asok on 08/12/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *re_password;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)login:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)registerUser:(id)sender;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    NSUserDefaults *defaulits=[NSUserDefaults standardUserDefaults];
    if (![defaulits boolForKey:@"registerd"])
    {
        
        NSLog(@"not user register");
        _loginButton.hidden=YES;
        
    }
    else
    {
        NSLog(@"user register");
        _re_password.hidden=YES;
        _registerButton.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)login:(id)sender {
}

- (IBAction)registerUser:(id)sender
{
    if ([_email.text isEqualToString:@""] || [_password.text isEqualToString:@""] || [_re_password.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Ooooops" message:@"All field mandetory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self checkPasswordMatch];
    }
}


-(void)checkPasswordMatch
{
    
}
@end
